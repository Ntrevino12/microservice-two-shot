# Wardrobify

Team:

* Jeff Chang - Which microservice? I'll be handling the Hat microservice
* Person 2 - Nicholas Trevino (shoes microservice)

## Design

## Shoes microservice

when looking at my model I set the data to manunfacture,name, color, picture, and used binVo as a foriegn key and had a poller match the information in binvo from the bin model of wardrobe. One piece of informatino I had match was the href in order to keep the data bases from scewing since were accessing information from a seperate data base. That way when a shoe is created it is stored in the create id for the bin.

## Hats microservice



--BACKEND--

MODELS:
I created a Hat model and then a LocationVO model that referenced the Location model in the Wardrobe application and set the attributes to be the same as the Location model in Wardrobe. The Hat model has the attributes fabric, style_name, color, picture_url, and location.

POLLER.PY
We then set up the poller.py to grab the information from the Wardrobe library by requesting the content from the API http://wardrobe-api:8000/api/locations/, this information is then set in an infinite while loop in the poll function where we continue to pull data specifically from the wardrobe-api URL and reference the model attributes from Location to the microservices LocationVO. The LocationVO serves as a point of reference for data on locations so we can have a functioning microservice without needing to have all the data in one place. We also put the `from hats_rest.models import LocationVO` import AFTER the django setup so the poller can operate properly.

ADMIN AND SETTINGS SETUP:
We then set up our models in our admin.py files within our monolith and microservice, this way we can see the models within our Django database using the '/admin' pathing. Then in both of our settings.py files we had to update the 'INSTALLED_APPS' to make sure that our Config Class in our apps.py files were linked together.




API_VIEWS:
 We then set up the api_views.py and implemented "get", "post", "delete" functions and made it RESTful.

******Set specific '@require_https_methods'******

"GET" LIST OF ALL HATS OR ALL HATS IN ONE LOCATION:
The list hats API sets up the "get" by getting the location_vo_id from the url and pulls the hats from that specific location, else if the location_vo_id is None then it'll show ALL hats.

"POST" HAT:
If the request is "post" then it will take the JSON content body that is submitted and then try to match a location based off the location_href. Once the location_href is identified from the LocationVO database, then it is assigned to the content with the key 'location' with the values from the LocationVO database. (However, if it isn't found, it will throw back an error.) Then after the location data is assigned to the body of the content, a hat is created with the `Hat.objects.create(**content)` command and the hat is then pushed to the Hat database and the python is encoded with the HatDetailEncoder, so it's in useable JSON form.

******Set specific '@require_https_methods'******

"GET" SHOW HAT DETAIL:
For the 'get' method in the api_show_hat function, all it is doing is if the request is 'get', it returns a JsonResponse of that specific hat to the url id.

"DELETE" HAT:
The 'delete' method is identifying the hat based on id and running the delete method.




URLS:
Our urls are set up in both the project folder of the microservice to have a path for 'api' and to include the hat application 'api_urls'. In the actual 'api_urls' we have our paths set up for our list and show functions.




--REACT--

To start on the React side, we set up our HatList first and import our necessary methods "import React, { useEffect, useState} from 'react';" so we can utilize React and balance the updating functionality of useState and limit the times it updates with useEffect and an empty array, and "import { Link } from 'react-router-dom'" so we can link to another webpage.




HATLIST FUNCTION:
We create the HatList function and create a const array with the two elements: the current state, and a function to update the state. 'const [state, setState] = useState(initialState)'. We then asynchronously fetch the data from the url and await the response. If the response fetch is ok, then we await the JSON data from the response and then using the data, we access the hat key and set the status using setState method. We set up useEffect() to make sure that when we run the useState() method, everytime the state is updated, the components re-render, so the useEffect() keep the components from re-rendering.

LISTING EACH HAT:
To list all the hats with details, we need to setup a map. Since our hat list data is being fetched from fetchData and the response is ok, our const hats value is being set with setHats to the value of the json response `setHats(data.hats);` now we can reference hats in our map function and have it return a row that returns each attribute.

DELETE FUNCTION:
Set the url to the specific id of the hat, then we await a response that fetches the url and executes the DELETE method. We use 'window.location.reload()' to make sure that the window is refreshed when the item is deleted because we found out that although you delete the item, the item does not disappear from the page until refreshed.




HATFORM FUNCTION:
Similar to setting the state in the list function, we're setting the state for all the attributes on our hat. One thing we want to make sure we're separating is the two types of location data that we will be saving. One location state is for the drop down option, this state will be set as an empty array instead of the standard empty string for regular character fields.

Next we set up the variables that will handle the changing of values. With the code:
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
The `handleFabricChange` is referenced lower in the JSX section as an attribute on the input tag. It's referenced using `onChange={handleFabricChange}` which is saying when this input is changed (or has an event) it will run that function within the curly brackets. A few attributes to keep in mind are: 'value', 'name' and 'id'. For convention purposes, 'name' and 'id' want to be the same for less confusion, and the 'name' must match the key name that you have stored in your database.

EXAMPLE:
    {
        hat: {
            'fabric_type': 'cloth',
        }
    }

You cannot reference the name in the value as 'value={fabricType}', when submitted, the system will not know where to assign the data. It must match like 'value={fabric_type}'.

When the handleStateChange function is ran, it takes the value of the event that occurred at the target, and sets the value to the assigned variable. This is how the form is able to submit information to the database.

FETCHING DATA FOR DROP-DOWN LIST:
The fetchData will asynchronously fetch the url, and if the response is ok, it will await the JSON data that is pulled from the url and then set uses the setLocations state to the list of locations so it will be visible on the browser when the drop-down is selected.

DROP-DOWN LIST ON WEBPAGE:
When we try to access the list drop-down on the webpage, we need to utilize another map function. we first identify the attribute that we want to list (locations in this instance) and then we set the key to something unique like the href, and set the value to href because that is the value we want to upload into the database. The {location.closet_name} indicates what will be displayed on the actual drop-down as options.
EXAMPLE:
    {locations.map(location => {
        return (
            <option key={location.href} value={location.href}>
                {location.closet_name}
            </option>
        );
    })}

HANDLESUBMIT:
The handleSubmit function is attached to the form tag as an attribute 'onSubmit={handleSubmit}'. This function will run when that form is submitted. To prevent the server from loading a response from the server, we use 'event.preventDefault();' to keep it on the same page. We then set an empty object (named 'data') and assign the properties of said object with the property variable names that we created at the start of the function:

EXAMPLE:
    data.fabric = fabric;
    data.style_name = style_name;
    data.color = color;
    data.picture_url = picture_url;
    data.location = location;

We identify the location that we want to submit the data into, and we do that by setting up a string template literal `http://localhost:8090/${location}hats/`. This will take the location href and pass it into the entire url, we set up the fetchConfig so the system knows what method, what to do with the data being passed in (converting it to JSON) and then how to interpret the data with headers. We then await a fetch to try and 'post' our data into the designated url with 'fetch(hatsUrl, fetchConfig)' and if the response is ok then we reset all our form parameters with the following code:
    setFabric('');
    setStyleName('');
    setPictureUrl('');
    setLocation('');
    setColor('');

And since we're using useState() again in this function, we're using useEffect() to make sure we only render the function once.
