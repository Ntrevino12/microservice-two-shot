import React, { useEffect, useState} from 'react';
import { Link } from 'react-router-dom'

function ShoeList(){
    const [shoes, setShoes] = useState([]);
    const fetchData  = async () => {
      const listUrl = `http://localhost:8080/api/shoes/`;
      const response = await fetch(listUrl);

      if (response.ok) {
        const data = await response.json();
        console.log(data)
        setShoes(data.shoes);
      }
    }

    async function deleteShoe(shoe) {
        const deleteUrl = `http://localhost:8080/api/shoes/${shoe.id}`
        const response = await fetch(deleteUrl, {method:"delete"})
        if (response.ok) {
          console.log("You just deleted this shoe", response)
        }
        window.location.reload();


    }
    useEffect(() => {
        fetchData();}, []);

    return (
    <div className="container">
        <div className="d-grid gap-10 d-lg-flex justify-content-sm-center">
            <Link to="/shoe/new" className="btn btn-primary btn-lg px-4 gap-3">Click to add another</Link>
        </div>
        <div className="col-md-auto offset-md-4">
        {shoes.map((shoe,id) => {
          return (
            <div key={id} className="card w-50 border-dark mb-5 shadow">
              <img src={shoe.picture} className="card-img-top img-thumbnail-border" />
              <div className="card-body">
                <h5 className="card-title text-center">{shoe.name}</h5>
                <h6 className="card-subtitle mb-2 text-muted text-center">
                  {shoe.manufacturer}
                </h6>
                <p className="card-text text-center">
                  {shoe.color}
                </p>
              </div>
              <div className="card-footer">
                {shoe.bin}
              <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                  <button onClick={() => deleteShoe(shoe)} type="button" className="btn btn-danger btn-sm">delete</button>
                </div>
              </div>
            </div>
          );
        })}
        </div>
    </div>
    );
  }
export default ShoeList;
