import React, { useEffect, useState } from 'react';

function HatForm() {

    const [fabric, setFabric] = useState('');
    const [style_name, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.fabric = fabric;
        data.style_name = style_name;
        data.color = color;
        data.picture_url = picture_url;
        data.location = location;

        const hatsUrl = `http://localhost:8090/${location}hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const Response = await fetch(hatsUrl, fetchConfig);

        if (Response.ok) {


            setFabric('');
            setStyleName('');
            setPictureUrl('');
            setLocation('');
            setColor('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    function click() {
        window.location.href = 'http://localhost:3000/hats'
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form
                        onSubmit={handleSubmit}
                        id="create-conf-form">
                        <div className="form-floating mb-3">
                            <input
                                value={fabric}
                                onChange={handleFabricChange}
                                placeholder="Fabric"
                                required type="text"
                                name='fabric' id="fabric"
                                className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={style_name}
                                onChange={handleStyleNameChange}
                                placeholder="Name"
                                required type="text"
                                name='style_name'
                                id="style_name"
                                className="form-control" />
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={color}
                                onChange={handleColorChange}
                                placeholder="Color"
                                required type="text"
                                id="color"
                                className="form-control"
                                name="color" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={picture_url}
                                onChange={handlePictureUrlChange}
                                placeholder="Picture Url"
                                required type="url"
                                name='picture_url'
                                id="picture_url"
                                className="form-control" />
                            <label htmlFor="picture_url">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select
                                value={location}
                                onChange={handleLocationChange}
                                required name="state"
                                id="state"
                                className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button
                            onClick={click} className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default HatForm;
