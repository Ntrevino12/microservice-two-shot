import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './shoeList';
import ShoeForm from './shoeForm';
import HatList from './hatList';
import HatForm from './hatForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index path="" element={<MainPage />} />
          <Route path="shoes" element={<ShoeList shoes={props.shoes} />} />
          <Route path="shoe/new" element={<ShoeForm/>}/>
          <Route path="hats">
            <Route path="new" element={<HatForm />} />
            <Route path="" element={<HatList hats={props.hats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
