import { React } from 'react';
import { Link } from 'react-router-dom';


const MainPage = () => {

    return (
        <>
            <div className="card px-4 py-5 my-5 mt-3 text-center bg-info">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="https://images.unsplash.com/photo-1466992133056-ae8de8e22809?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1208&q=80" alt="" width="600" />
                <h1 className="display-5 text-white fw-bold">WARDROBIFY!</h1>
                <div className="text-white col-lg-6 mx-auto">
                    <p className="lead mb-4">
                        Steeze your organization game with Wardrobify!
                        This is the last app you'll ever need to organize your shoes and hats!
                    </p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <Link to="/shoes/" className="btn btn-dark btn-lg mx-5 px-4 gap-3 text-white fw-bold">See Shoes</Link>
                        <Link to="/hats/" className="btn btn-dark btn-lg mx-5 px-4 gap-3 text-white fw-bold">See Hats</Link>
                    </div>

                </div>
            </div>


        </>
    );
}

export default MainPage;
